const request = require('supertest');
const { Rental } = require('../../models/rental');
const { Movie } = require('../../models/movie');
const { User } = require('../../models/user');
const mongoose =  require('mongoose');

describe('/api/returns', () => {
    let server;
    let customerID;
    let movieID;
    let rental;
    let movie;
    let token;

    const exec = () => {
        return request(server)
        .post('/api/returns')
        .set('x-auth-token', token)
        .send({customerID, movieID});  
    };

    beforeEach(async () => {
        server = require('../../index');

        customerID = mongoose.Types.ObjectId();
        movieID = mongoose.Types.ObjectId();
        
        token = new User().generateAuthToken();

        movie = new Movie({
            _id : movieID,
            title : '12345',
            genre : {
                name : '1234567890'
            },
            numberInStock : 1,
            dailyRentalRate : 3
        });

        await movie.save();

        rental = new Rental({
            customer : {
                _id : customerID,
                name : '1234',
                phone : '12345'
            },
            movie : {
                _id : movieID,
                title : '12345',
                dailyRentalRate : 3
            }
        });
        await rental.save();
    });

    afterEach(async () => {
        await server.close();
        await Rental.deleteMany()
        await Movie.deleteMany()
    });

    it('should return 401 if client not logged in', async() => {
        token = ''

        const res = await exec();

        expect(res.status).toBe(401);
    });

    it('should return 400 if customerID not provided', async() => {
        customerID = '';
        
        const res = await exec();

        expect(res.status).toBe(400);
    });

    it('should return 400 if movieID not provided', async() => {
        movieID = '';
        
        const res = await exec();

        expect(res.status).toBe(400);
    });

    it('should return 404 if no rental found for this customer/movie', async() => {   
        await Rental.deleteMany();
        
        const res = await exec();

        expect(res.status).toBe(404);
    });

    it('should return 400 if no rental already processed', async() => {  
        rental.dateReturned = new Date();
        await rental.save();
        
        const res = await exec();

        expect(res.status).toBe(400);
    });

    it('should return 200 if valid request', async() => {  
        const res = await exec();

        expect(res.status).toBe(200);
    });

    it('should set the returnDate if input is valid', async() => {  
        const res = await exec();

        const rentalInDb = await Rental.findById(rental._id);

        const diff = new Date() - rentalInDb.dateReturned;

        expect(diff).toBeLessThan(10 * 1000);
    });

    it('should calculate the rental fee if input is valid', async() => {  
        rental.dateOut = new Date(new Date().getTime() - ( (24 * 60 * 60 * 1000) * 7 ));

        await rental.save()
        
        const res = await exec();

        const rentalInDb = await Rental.findById(rental._id);

        expect(rentalInDb.rentalFee).toBe(21);
    });

    it('should increase the stock of the movie if input is valid', async() => {  
        const res = await exec();

        const movieInDB = await Movie.findById(movieID);

        expect(movieInDB.numberInStock).toBe(2);
    });

    it('should return the rental if input is valid', async() => {  
        const res = await exec();
        
        expect(Object.keys(res.body)).toEqual(expect.arrayContaining([
            'customer', 'movie', 'dateOut', 'dateReturned', 'rentalFee'
        ]));
    });
});