
const config = require('config');
const { Genre } = require('../../models/genre');
const { User } = require('../../models/user');
const request = require('supertest');

let server;

describe('auth middelware', () => {
    beforeEach(() => { server = require('../../index'); });
    afterEach(async() => { 
        await server.close();
        await Genre.deleteMany();
    });

    let token;

    const exec = () => {
        return request(server)
                .post('/api/genres')
                .set('x-auth-token', token)
                .send({ name : 'genre1'})
    }; 

    beforeEach(() => { token = new User().generateAuthToken(); })

    it('should return 401 if no token provided', async() => {
        token = '';
        
        const res = await exec();

        expect(res.status).toBe(401);
    });

    it('should return 400 if an invalid token is passed', async() => {
        token = 'a';
        
        const res = await exec();

        expect(res.status).toBe(400);
    });

    it('should return 200 if a valid token is passed', async() => {        
        const res = await exec();

        expect(res.status).toBe(200);
    });
});