const auth =  require('../middleware/auth');
const { Customer, validate } = require('../models/customer');
const express = require('express');
const router = express.Router();
const validateObjectId = require('../middleware/validateObjectId');
const admin = require('../middleware/admin');

router.get('/', async(req, res) => {
    const customers = await getCustomers();
    return res.send(customers);
});

router.get('/:id', validateObjectId, async(req, res) => {
    const customer = await getCustomerById(req.params.id);
    if(!customer)
        return res.status(404).send('The customer with the following ID not found !');
    
    return res.send(customer);
});

router.post('/', auth, async(req, res) => {
    const { error } = validate(req.body);
    if(error)
        return res.status(400).send(error.details[0].message);
    
    const customer = await createCustomer(req.body);
    return res.send(customer);
});

router.put('/:id', [ auth, validateObjectId], async(req, res) => {
    const { error } = validate(req.body);
    if(error)
        return res.status(400).send(error.details[0].message);
    
    const customer = await updateCustomer(req.params.id, req.body);
    if(!customer)
        return res.status(404).send('The customer with the following ID not found !');

    return res.send(customer);
});

router.delete('/:id', [ auth, validateObjectId, admin], async(req, res) => {
    const customer = await deleteCustomer(req.params.id);
    if(!customer)
        return res.status(404).send('The customer with the following ID not found !');

    return res.send(customer);
});

async function getCustomers(){
    try {
        const customers = await Customer.find();
        return customers;
    } catch (error) {
        return error.message;
    }
}

async function getCustomerById(id){
    try {
        const customer = await Customer.findById(id);
        return customer;
    } catch (error) {
        return error.message;
    }
}

async function createCustomer(data){
    const customer = new Customer({
        isGold : data.isGold,
        name : data.name,
        phone : data.phone
    });
    try {
        await customer.save(data);
        return customer;
    } catch (error) {
        return error.message;
    }
}

async function updateCustomer(id, data){
    try {
        customer = await Customer.findByIdAndUpdate(id, data, { new : true });
        return customer;
    } catch (error) {
        return error.message;
    }
}

async function deleteCustomer(id){
    try {
        customer = await Customer.findByIdAndRemove(id);
        return customer;
    } catch (error) {
        return error.message;
    }
}

module.exports = router;