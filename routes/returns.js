const Joi = require('@hapi/joi');
const express =  require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const validate = require('../middleware/validate');
const { Rental } = require('../models/rental');
const { Movie } = require('../models/movie');

router.post('/', [auth, validate(validateReturn)], async (req, res) => {
    const rental = await Rental.lookup(req.body.customerID, req.body.movieID);

    if(!rental)
        return res.status(404).send('Rental was not found.');
    
    if(rental.dateReturned)
        return res.status(400).send('Rental already processed.'); 
        
    rental.return();
    await rental.save();

    await Movie.updateOne({_id : rental.movie._id},{
        $inc : { numberInStock : 1 }
    })
    
    return res.send(rental);
});

function validateReturn(payload){
    const schema = Joi.object({
        customerID : Joi.objectId().required(),
        movieID : Joi.objectId().required()
    });

    return schema.validate(payload);
}

module.exports = router;