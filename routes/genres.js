const asyncMiddleware = require('../middleware/async');
const auth =  require('../middleware/auth');
const admin =  require('../middleware/admin');
const validator = require('../middleware/validate');
const { Genre, validate } = require('../models/genre');
const express = require('express');
const router = express.Router();
const validateObjectId = require('../middleware/validateObjectId');

router.get('/', async(req, res, next) => {
        const genres = await getGenres();
        return res.send(genres); 
});

router.get('/:id',validateObjectId , async(req, res) => {
        const genre = await getGenreById(req.params.id);
        if(!genre)
            return res.status(404).send('The genre was not found');
    
        return res.send(genre);
});

router.post('/', [auth, validator(validate)], async(req, res) => {     
        const genre = await createGenre(req.body);
    
        return res.send(genre); 
});

router.put('/:id', [ auth, validateObjectId, validator(validate) ], async(req, res) => { 
        const genre = await updateGenre(req.params.id, req.body);
        if(!genre)
            return res.status(404).send('The genre was not found');
    
        return res.send(genre); 
});

router.delete('/:id', [ auth, validateObjectId, admin ], async(req, res) => {
        const genre = await deleteGenre(req.params.id);
        if(!genre)
            return res.status(404).send('The genre was not found');
    
        return res.send(genre);
});

async function getGenres(){
        const genres = await Genre.find();
        return genres;
}

async function getGenreById(id){
        const genre = await Genre.findById(id);
        return genre;
}

async function createGenre(data){
    const genre =  new Genre({
        name : data.name
    });

    try {
        await genre.save();
        return genre;

    } catch (error) {
        return error.message;
    }
} 

async function updateGenre(id, data){
    try {
        const result = await Genre.findByIdAndUpdate(id, { name : data.name }, { new : true});
        return result;

    } catch (error) {
        return error.message;
    }
}

async function deleteGenre(id){
    try {
        const result = await Genre.findByIdAndRemove(id);
        return result;

    } catch (error) {
        return error.message;
    }
} 

module.exports = router;