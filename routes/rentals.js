const auth =  require('../middleware/auth');
const mongoose = require('mongoose');
const { Movie } = require('../models/movie');
const { Customer } = require('../models/customer');
const { Rental, validate } = require('../models/rental');
const express = require('express');
const router = express.Router();

router.get('/', async(req, res) => {
    const rentals = await getRentals();
    return res.send(rentals);
});

router.post('/', auth, async(req, res) => {
    try {
        const { error } = validate(req.body);
        if(error)
            return res.status(400).send(error.details[0].message);
        
        const customer = await checkCustomerExistance(req.body.customerID);
        if(!customer)
            return res.status(400).send('Invalid Customer !');
    
        const movie = await checkMovieExistance(req.body.movieID);
        if(!movie)
            return res.status(400).send('Invalid Movie !');
        if(movie.numberInStock === 0)
            return res.status(400).send('Movie not in stock.');
    
        
        const rental = await createRental(customer, movie);
        return res.send(rental);  
    } catch (error) {
        return res.send(error);
    }   
});

async function getRentals(){
    try {
        const rentals = await Rental.find();
        return rentals;
    } catch (error) {
        return error.message
    }
}

async function createRental(customer, movie){
    let rental = new Rental({
        customer : new Customer({
            _id : customer._id,
            name : customer.name,
            isGold : customer.isGold,
            phone : customer.phone
        }),
        movie : new Movie({
            _id : movie._id,
            title : movie.title,
            dailyRentalRate : movie.dailyRentalRate
        }),
    });
    // const session = await mongoose.startSession();
    // session.startTransaction();
    try {

        // rental = await rental.save({ session });
        await rental.save();

        movie.numberInStock--;
        // await movie.save({ session });
        await movie.save();

        // await session.commitTransaction();

        return rental
    } catch (error) {
        return error.message;
    }
}

async function checkCustomerExistance(customerID){
    try {
        const customer = await Customer.findById(customerID);
        return customer;
    } catch (error) {
        return error.message;
    }
}

async function checkMovieExistance(movieID){
    try {
        const movie = await Movie.findById(movieID);
        return movie;
    } catch (error) {
        return error.message;
    }
}

module.exports = router;