const Joi = require('@hapi/joi');
const express = require('express');
const router = express.Router();

const genres = [
    { id : 1 , type : "Action"},
    { id : 2 , type : "Mystery"},
    { id : 3 , type : "Romance"},
];

router.get('/', (req, res) => {
    res.send(genres);
});

router.get('/:id', (req, res) => {
    const genre = genres.find(g => g.id === parseInt(req.params.id));
    if(!genre)
        return res.status(404).send('The genre was not found');

    return res.send(genre);
});

router.post('/', (req, res) => {
    const { error } = validate(req.body);
    if(error)
        return res.status(400).send(error.details[0].message);
    
    const genre = {
        id : genres.length + 1,
        type : req.body.type
    };
    genres.push(genre);

    return res.send(genre);
});

router.put('/:id', (req, res) => {
    const genre = genres.find(g => g.id === parseInt(req.params.id));
    if(!genre)
        return res.status(404).send('The genre was not found');

    const { error } = validate(req.body);
    if(error)
        return res.status(400).send(error.details[0].message);

    genre.type = req.body.type;

    return res.send(genre);
});

router.delete('/:id', (req, res) => {
    const genre = genres.find(g => g.id === parseInt(req.params.id));
    if(!genre)
        return res.status(404).send('The genre was not found');
    
    const index = genres.indexOf(genre);
    genres.splice(index, 1);

    return res.send(genre);
});

function validate(genre){
    const schema = Joi.object({
        type : Joi.string()
                  .min(3)
                  .required()
    });

    return schema.validate(genre);
}

module.exports = router;