const auth =  require('../middleware/auth');
const { Movie, validate } = require('../models/movie');
const { Genre } = require('../models/genre');
const express = require('express');
const router = express.Router();
const validateObjectId = require('../middleware/validateObjectId');
const admin = require('../middleware/admin');

router.get('/', async(req, res) => {
    const movies = await getMovies();
    return res.send(movies);
});

router.get('/:id', validateObjectId, async(req, res) => {
    const movie = await getMovieById(req.params.id);
    if(!movie)
        return res.status(404).send('The movie with the given ID was not found !');

    return res.send(movie);
});

router.post('/', auth, async(req, res) => {
    const { error } = validate(req.body);
    if(error)
        return res.status(400).send(error.details[0].message);

    const genre = await getGenreById(req.body.genreId);
    if(!genre)
        return res.status(404).send('genre not found !');
    

    const movie = await createMovie(req.body, genre);

    return res.send(movie);
});

router.put('/:id', [ auth, validateObjectId], async(req, res) => {
    const { error } = validate(req.body);
    if(error)
        return res.status(400).send(error.details[0].message);

    const genre = await getGenreById(req.body.genreId);
    if(!genre)
        return res.status(404).send('genre not found !');

    const movie = await updateMovie(req.params.id, req.body, genre);
    if(!movie)
        return res.status(404).send('The movie with the given ID was not found !'); 

    return res.send(movie);
});

router.delete('/:id', [ auth, validateObjectId, admin], async(req, res) => {
    const movie = await deleteMovie(req.params.id);
    if(!movie)
        return res.status(404).send('The movie with the given ID was not found !'); 

    return res.send(movie);
});

async function getMovies(){
    try {
        const movies = await Movie.find();
        return  movies;
    } catch (error) {
        return error.message;
    }
}

async function getMovieById(id){
    try {
        const movie = await Movie.findById(id);
        return  movie;
    } catch (error) {
        return error.message;
    }
}

async function getGenreById(id){
    try {
        const genre = await Genre.findById(id);
        return  genre;
    } catch (error) {
        return error.message;
    }
}

async function createMovie(data, genre){
    const movie = new Movie({
        title : data.title,
        genre : {
            _id : genre._id,
            name : genre.name
        },
        numberInStock : data.numberInStock,
        dailyRentalRate : data.dailyRentalRate
    });
    try {
        await movie.save();
        return  movie;
    } catch (error) {
        return error.message;
    }
}

async function updateMovie(id, data, genre){
    let movie = {
        title : data.title,
        genre : {
            _id : genre._id,
            name : genre.name
        },
        numberInStock : data.numberInStock,
        dailyRentalRate : data.dailyRentalRate
    };
    try {
        movie = await Movie.findByIdAndUpdate(id, movie, { new : true });
        return  movie;
    } catch (error) {
        return error.message;
    }
}

async function deleteMovie(id){
    try {
        const movie = await Movie.findByIdAndDelete(id);
        return  movie;
    } catch (error) {
        return error.message;
    }
}

module.exports = router;
