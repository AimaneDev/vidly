const winston = require('winston');
// require('winston-mongodb');
require('express-async-errors');


module.exports = function(){
    // process.on('uncaughtException', (ex) => {
    //         console.log('WE GOT AN UNCAUGHT EXEPTION');
    //         winston.error(ex.message, ex);
    // });


    // Enable exception handling when you create your logger.
    winston.createLogger({exceptionHandlers: 
        [
            new winston.transports.Console({ colorize : true , prettyPrint : true }),
            new winston.transports.File({ filename: 'uncaughtException.log' })]
    });

    process.on('unhandledRejection', (ex) => {
        throw ex
    });

    winston.add(new winston.transports.File({ filename: 'errors.log' }));
    // winston.add(new winston.transports.MongoDB({
    //             db: 'mongodb://localhost:27017/vidly',
    //             collection: 'log',
    //             level: 'error',
    //             storeHost: true,
    //             capped: true,
    // }));
}