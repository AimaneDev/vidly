const mongoose = require('mongoose');
const Joi = require('@hapi/joi');

const genreSchema = mongoose.Schema({
    name : {
        type:String,
        minlength : 5,
        maxlenght : 50,
        trim : true,
        require : true
    }
});

const Genre = mongoose.model('Genre', genreSchema);

function validate(genre){
    const schema = Joi.object({
        name : Joi.string()
                  .min(5)
                  .max(50)
                  .required()
    });

    return schema.validate(genre);
}

module.exports.genreSchema = genreSchema;
module.exports.Genre = Genre;
module.exports.validate = validate;