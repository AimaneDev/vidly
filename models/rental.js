const mongoose = require('mongoose');
const Joi = require('@hapi/joi');

const rentalSchema = new mongoose.Schema({
    customer : {
        type : new mongoose.Schema({
            name: {
                type: String,
                required: true,
                minlength: 3,
                maxlength: 50
            },
            isGold: {
                type: Boolean,
                default: false
            },
            phone: {
                type: String,
                required: true,
                minlength: 5,
                maxlength: 50
            }      
        }),
        required : true
    },
    movie : {
        type : new mongoose.Schema({
            title: {
                type: String,
                required: true,
                trim: true, 
                minlength: 5,
                maxlength: 255
            },
            dailyRentalRate: { 
                type: Number, 
                required: true,
                min: 0,
                max: 255
            }   
        }),
        required : true
    },
    dateOut: { 
        type: Date, 
        required: true,
        default: Date.now
    },
    dateReturned: { 
        type: Date
    },
    rentalFee: { 
        type: Number, 
        min: 0
    }
});

rentalSchema.statics.lookup = function(customerID, movieID){
    return Rental.findOne({
        'customer._id': customerID,
        'movie._id' : movieID 
    });
}

rentalSchema.methods.return = function(){
    this.dateReturned = new Date();
    
    const numberOfDays = Math.round(Math.round(this.dateReturned - this.dateOut) / (1000 * 60 * 60 * 24));
    this.rentalFee = numberOfDays * this.movie.dailyRentalRate;
}

const Rental = mongoose.model('Rental', rentalSchema);

function validate(rental){
    const schema = Joi.object({
        customerID : Joi.objectId().required(),
        movieID : Joi.objectId().required(),
    });

    return schema.validate(rental);
}

module.exports.Rental = Rental;
module.exports.validate = validate;