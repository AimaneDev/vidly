const Joi = require('@hapi/joi');
const mongoose = require('mongoose');

const customerSchema = new mongoose.Schema({
    isGold : {
        type : Boolean,
        default : false
    },
    name : {
        type : String,
        require : true,
        minlength : 3,
        maxlength : 50,
        trim : true
    },
    phone : {
        type : String,
        minlength : 8,
        maxlength : 15,
        require : true
    }
});

const Customer =  mongoose.model('Customer', customerSchema);

function validate(customer){

    const schema = Joi.object({
        isGold : Joi.boolean()
                    .default(false),
        name : Joi.string()
                  .required()
                  .min(3)
                  .max(50),
        phone : Joi.string()
                   .required()
                   .min(8)
                   .max(15)
    });

    return schema.validate(customer);
}

module.exports.Customer = Customer;
module.exports.validate = validate;